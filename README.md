# Drupal test project

This project provides a test work for Lemberg.

## Up project via Lando

First you need to install [docker](https://www.docker.com/get-started) and [lando](https://docs.devwithlando.io).

After that you can start an app in cloned project:

```
lando start
```

Next step is to launch script with project installation:

```
lando ssh
bash scripts/site-install-via-lando.sh
```

And voila - the project is installed. You can find urls to the site, by typing command:

```
lando info
```

# Description of the work done

##### Create a custom page(via code) with a list of nodes(/custom-page), nodes should be grouped by content type.

####### Explain:
Created a custom page (via code) in which loaded the view block with a list of all nodes grouped by type. Specially I did not write the logic of queries to the database in the controller since I believe that implementation of the views carries less bugs, easier support, and it is Drupal way.
`web/modules/custom/lemberg_base/src/Controller/NodesListController.php`

---
##### Create Paragraph Banner(fields: image, title), add it to the News content type. Add new region Hero(above the page title) in your theme and display created Paragraph there.

####### Explain:
Created content type "News", added to this type of paragraph "Banner" and disabled it in the "Manage display" of this content type. Added region "Hero" to lemberg.info.yml. Also, with the help of the views, I posted this paragraph from the entity with the context filter and showed it in the "Hero" region. Did not use a custom block plugin, preprocess or other solution for the reasons mentioned in the first paragraph.

---
##### Add a custom Block plugin to user pages, that displays username and role, depending on the user page it is displayed on.
  
####### Explain:
Created a custom block of the plugin in which brought out the username and role of the user. Displayed on user's pages.
`web/modules/custom/lemberg_base/src/Plugin/Block/UserInfo.php`

---
##### Alter the existing route. Change path from /user/login to /global/login. Add menu item at the main menu via code.
I wrote by RouteSubscriber in which changed the user.login route and changed the route path to /global/login. In order not to have 404 errors on the page /user/login, I wrote my Middleware, which made the redirect to the new path at the earliest stage.
I programmatically created two items in the main menu.
`web/modules/custom/lemberg_base/src/Routing/RouteSubscriber.php`
`web/modules/custom/lemberg_base/src/StackMiddleware/MiddlewareLogin.php`

---
##### Create a custom form at admin/config/environment-settings. Add “Welcome Text” textarea, add “Promotion file” file upload field. Make sure that data saved here is independent from Drupal configs and can be specific per site installation.
I created the configuration form admin `/config/environment-settings`, and the item in the menu for this form. The form is based on the State API, so the data is stored in the database, but not exported with the configuration.
`web/modules/custom/lemberg_base/src/Form/EnvironmentSettingsFrom.php`

---
##### Create a new Block plugin that displays the latest node of a specific type. Make sure a separate block is created automatically for each existing/new content type in the system.
I created an additional configuration for the custom block plugin, in which the type of entity is selected, and according to this type, all the pages display the last entity of this type. Everything is automatically added and changed.
`web/modules/custom/lemberg_base/src/Plugin/Block/LatestNode.php`

---
##### Create a custom “Post” entity type, with Description field. Export entity type configs in to the module folder.
##### Create a new “Latest Posts” Views block, that shows 3 latest Post nodes, set it to show at User pages.
##### Create a custom Views Access plugin, which would ensure that “Latest Posts” are only shown for the user, if they have created at least 1 “Post” node, depending on the user page it is displayed on.
Created custom “Post” entity type, with Description field.
The configuration of this entity is stored in the module. I created a presentation based on this kind of entity, which shows the last 3 entities on the `/user/*` pages. Also created is a custom Views Access plugin, which displays this block only if the user has created at least one entity of this type.
`web/modules/custom/post_entity/`
`web/modules/custom/views_access/`

###* If something went wrong or something wrong, please let me know.
###* I tried very hard

## Thanks.