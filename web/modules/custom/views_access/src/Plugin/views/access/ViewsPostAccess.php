<?php

namespace Drupal\views_access\Plugin\views\access;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Class ViewsCustomAccess.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *     id = "ViewsPostAccess",
 *     title = @Translation("Only for users who have created a post"),
 *     help = @Translation("Add custom logic to access() method"),
 * )
 */
class ViewsPostAccess extends AccessPluginBase {

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    AccountProxyInterface $currentUser,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    return $this->t('Customised Settings');
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    $storage = $this->entityTypeManager->getStorage('post_entity');
    $query = $storage->getQuery()
      ->condition('status', 1)
      ->pager(10);
    $nids = $query->execute();

    $autor_ids = [];
    foreach ($nids as $nid) {
      $entity = $storage->load($nid);
      $autor_ids[] = $entity->getOwner()->id();
    }

    if (in_array($this->currentUser->id(), $autor_ids)) {
      return TRUE;
    }
    else {
      return FALSE;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    // TODO: Implement alterRouteDefinition() method.
  }

}
