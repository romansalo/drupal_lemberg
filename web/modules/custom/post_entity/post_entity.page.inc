<?php

/**
 * @file
 * Contains post_entity.page.inc.
 *
 * Page callback for Post entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Post entity templates.
 *
 * Default template: post_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_post_entity(array &$variables) {
  // Fetch PostEntity Entity Object.
  $post_entity = $variables['elements']['#post_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
