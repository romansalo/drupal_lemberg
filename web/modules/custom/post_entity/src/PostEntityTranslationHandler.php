<?php

namespace Drupal\post_entity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for post_entity.
 */
class PostEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
