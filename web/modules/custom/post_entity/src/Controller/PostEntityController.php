<?php

namespace Drupal\post_entity\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\post_entity\Entity\PostEntityInterface;

/**
 * Class PostEntityController.
 *
 *  Returns responses for Post entity routes.
 */
class PostEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Post entity  revision.
   *
   * @param int $post_entity_revision
   *   The Post entity  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($post_entity_revision) {
    $post_entity = $this->entityManager()->getStorage('post_entity')->loadRevision($post_entity_revision);
    $view_builder = $this->entityManager()->getViewBuilder('post_entity');

    return $view_builder->view($post_entity);
  }

  /**
   * Page title callback for a Post entity  revision.
   *
   * @param int $post_entity_revision
   *   The Post entity  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($post_entity_revision) {
    $post_entity = $this->entityManager()->getStorage('post_entity')->loadRevision($post_entity_revision);
    return $this->t('Revision of %title from %date', ['%title' => $post_entity->label(), '%date' => format_date($post_entity->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Post entity .
   *
   * @param \Drupal\post_entity\Entity\PostEntityInterface $post_entity
   *   A Post entity  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(PostEntityInterface $post_entity) {
    $account = $this->currentUser();
    $langcode = $post_entity->language()->getId();
    $langname = $post_entity->language()->getName();
    $languages = $post_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $post_entity_storage = $this->entityManager()->getStorage('post_entity');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $post_entity->label()]) : $this->t('Revisions for %title', ['%title' => $post_entity->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all post entity revisions") || $account->hasPermission('administer post entity entities')));
    $delete_permission = (($account->hasPermission("delete all post entity revisions") || $account->hasPermission('administer post entity entities')));

    $rows = [];

    $vids = $post_entity_storage->revisionIds($post_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\post_entity\PostEntityInterface $revision */
      $revision = $post_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $post_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.post_entity.revision', ['post_entity' => $post_entity->id(), 'post_entity_revision' => $vid]));
        }
        else {
          $link = $post_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.post_entity.translation_revert', ['post_entity' => $post_entity->id(), 'post_entity_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.post_entity.revision_revert', ['post_entity' => $post_entity->id(), 'post_entity_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.post_entity.revision_delete', ['post_entity' => $post_entity->id(), 'post_entity_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['post_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
