<?php

namespace Drupal\post_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Post entity entities.
 *
 * @ingroup post_entity
 */
interface PostEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Post entity name.
   *
   * @return string
   *   Name of the Post entity.
   */
  public function getName();

  /**
   * Sets the Post entity name.
   *
   * @param string $name
   *   The Post entity name.
   *
   * @return \Drupal\post_entity\Entity\PostEntityInterface
   *   The called Post entity entity.
   */
  public function setName($name);

  /**
   * Gets the Post entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Post entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Post entity creation timestamp.
   *
   * @param int $timestamp
   *   The Post entity creation timestamp.
   *
   * @return \Drupal\post_entity\Entity\PostEntityInterface
   *   The called Post entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Post entity published status indicator.
   *
   * Unpublished Post entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Post entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Post entity.
   *
   * @param bool $published
   *   TRUE to set this Post entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\post_entity\Entity\PostEntityInterface
   *   The called Post entity entity.
   */
  public function setPublished($published);

  /**
   * Gets the Post entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Post entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\post_entity\Entity\PostEntityInterface
   *   The called Post entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Post entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Post entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\post_entity\Entity\PostEntityInterface
   *   The called Post entity entity.
   */
  public function setRevisionUserId($uid);

}
