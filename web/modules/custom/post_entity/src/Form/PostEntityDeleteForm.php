<?php

namespace Drupal\post_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Post entity entities.
 *
 * @ingroup post_entity
 */
class PostEntityDeleteForm extends ContentEntityDeleteForm {


}
