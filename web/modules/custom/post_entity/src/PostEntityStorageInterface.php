<?php

namespace Drupal\post_entity;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\post_entity\Entity\PostEntityInterface;

/**
 * Defines the storage handler class for Post entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Post entity entities.
 *
 * @ingroup post_entity
 */
interface PostEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Post entity revision IDs for a specific Post entity.
   *
   * @param \Drupal\post_entity\Entity\PostEntityInterface $entity
   *   The Post entity entity.
   *
   * @return int[]
   *   Post entity revision IDs (in ascending order).
   */
  public function revisionIds(PostEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Post entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Post entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\post_entity\Entity\PostEntityInterface $entity
   *   The Post entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(PostEntityInterface $entity);

  /**
   * Unsets the language for all Post entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
