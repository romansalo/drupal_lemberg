<?php

namespace Drupal\post_entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Post entity entity.
 *
 * @see \Drupal\post_entity\Entity\PostEntity.
 */
class PostEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\post_entity\Entity\PostEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished post entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published post entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit post entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete post entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add post entity entities');
  }

}
