<?php

namespace Drupal\post_entity;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\post_entity\Entity\PostEntityInterface;

/**
 * Defines the storage handler class for Post entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Post entity entities.
 *
 * @ingroup post_entity
 */
class PostEntityStorage extends SqlContentEntityStorage implements PostEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(PostEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {post_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {post_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(PostEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {post_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('post_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
