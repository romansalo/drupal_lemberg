<?php

namespace Drupal\lemberg_base\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\State;

/**
 * Class EnvironmentSettingsFrom.
 *
 * @package Drupal\lemberg_base\Form
 */
class EnvironmentSettingsFrom extends FormBase {

  /**
   * State service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * EnvironmentSettingsFrom constructor.
   */
  public function __construct(State $state) {
    $this->state = $state;
  }

  /**
   * EnvironmentSettingsFrom create method.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'environment_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['environment'] = [
      '#type' => 'details',
      '#title' => $this->t('Environment settings'),
    ];
    $form['environment']['welcome'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Welcome text'),
      '#description' => $this->t('This message will be show after login.'),
      '#default_value' => $this->state->get('welcome'),
    ];
    $form['environment']['promotion_file'] = [
      '#type' => 'managed_file',
      '#name' => 'my_file',
      '#title' => $this->t('Promotion file'),
      '#default_value' => $this->state->get('promotion_file'),
      '#size' => 20,
      '#description' => $this->t('You can upload files of the following formats: pdf, txt, doc, docx'),
      '#upload_validators' => [
        'file_validate_extensions' => ['pdf docx txt doc'],
      ],
      '#upload_location' => 'public://promotion_files/',
    ];
    $form['environment']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = [
      'welcome' => $form_state->getValue('welcome')['value'],
      'promotion_file' => $form_state->getValue('promotion_file'),
    ];
    $this->state->setMultiple($values);
  }

}
