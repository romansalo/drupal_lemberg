<?php

namespace Drupal\lemberg_base\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Latest Node' Block.
 *
 * @Block(
 *   id = "latest_node",
 *   admin_label = @Translation("Latest node block"),
 * )
 */
class LatestNode extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates an instance of the plugin.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * ListOfNodesBlock class constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $types_list = $this->getNodeType();

    $form['latest_node_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Please select a type that displays the latest node of a specific type.'),
      '#options' => $types_list,
      "#empty_option" => $this->t('- Select -'),
      '#default_value' => isset($config['latest_node_type']) ? $config['latest_node_type'] : '',
    ];

    return $form;
  }

  /**
   * Get node types.
   */
  private function getNodeType() {
    $types = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();
    $types_list = array_map('ucfirst', array_keys($types));

    return $types_list;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['latest_node_type'] = $values['latest_node_type'];

    $this->setConfigurationValue('node_types', $this->getNodeType());
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $latest_node_type = $config['latest_node_type'];
    $types = $config['node_types'];

    if (is_null($latest_node_type)) {
      return [];
    }
    else {
      $storage = $this->entityTypeManager->getStorage('node');
      $view_builder = $this->entityTypeManager->getViewBuilder('node');

      $nid = $storage->getQuery()
        ->condition('status', 1)
        ->condition('type', $types[$latest_node_type])
        ->sort('created', 'DESC')
        ->range(0, 1)
        ->execute();

      if ($nid) {
        $node = $this->entityTypeManager->getStorage('node')->loadMultiple($nid);
        $build = $view_builder->view(array_shift($node), 'teaser');

        return [
          '#theme' => 'latest_node_block',
          '#item' => $build,
        ];
      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
