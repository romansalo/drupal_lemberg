<?php

namespace Drupal\lemberg_base\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'User Info' Block.
 *
 * @Block(
 *   id = "user_info",
 *   admin_label = @Translation("User info block"),
 * )
 */
class UserInfo extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Stores an entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The entity storage for User entity type.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * Stores the current logged in user or anonymous account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentAccount;

  /**
   * Stores the current user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * UserInfo constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManager $entity_type_manager,
    AccountProxyInterface $current_account
  ) {
    // Get default values.
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Get user entity tools.
    $this->entityTypeManager = $entity_type_manager;
    $this->userStorage = $entity_type_manager->getStorage('user');

    // Get user info.
    $this->currentAccount = $current_account;
    $this->currentUser = $this->userStorage->load($this->currentAccount->id());
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      ['#markup' => $this->t('@label', ['@label' => 'Name: '])],
      ['#markup' => $this->currentUser->getUsername()],
      [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#title' => 'Roles',
        '#items' => $this->currentUser->getRoles(),
        '#attributes' => ['class' => 'roles-list'],
        '#wrapper_attributes' => ['class' => 'container'],
      ],
    ];
  }

}
