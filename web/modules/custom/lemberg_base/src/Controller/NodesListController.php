<?php

namespace Drupal\lemberg_base\Controller;

use Drupal\block\Entity\Block;
use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the Nodes list.
 */
class NodesListController extends ControllerBase {

  /**
   * Returns a views block with list of nodes.
   *
   * @return array
   *   A simple renderable array.
   */
  public function pageNodeList() {
    $bid = 'views_block__node_list_block_node_list';

    $block = Block::load($bid);
    $block_view = $this->entityTypeManager()
      ->getViewBuilder('block')
      ->view($block);

    return [
      '#theme' => 'nodes_list',
      '#items' => $block_view,
    ];
  }

}
