#!/bin/bash

#---------------------DESCRIPTION--------------------
# This script helps to import yaml content from modules.
# Example: bash scripts/yaml-content-import-modules.sh web/modules/content
#
# Syntax
# bash yaml-content-import-modules.sh PATH_TO_YAML_MODULES_FOLDER
#----------------------------------------------------

# Check count of arguments.
if [ $# != 1 ]; then
    echo "You must specify 1 arguments: files folder parameters. Example: bash scripts/yaml-content-import-modules.sh web/modules/content/"
    exit 0
fi

for f in `find ${1} -maxdepth 1 -mindepth 1 -type d`;
do
  module=${f#"$1"}
  bin/drush ycim "${module}" | echo "bin/drush ycim ${module}"
done
