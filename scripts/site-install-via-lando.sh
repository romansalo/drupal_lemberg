#!/bin/sh

composer install

# Execute site install with default settings using Drupal console.
bin/drupal site:install standard \
--langcode="en" \
--db-type="mysql" \
--db-host="database" \
--db-name="drupal8" \
--db-user="drupal8" \
--db-pass="drupal8" \
--db-port="3306" \
--site-name="Drupal test" \
--site-mail="admin@example.com" \
--account-name="admin" \
--account-pass="admin" \
--account-mail="admin@example.com"

# Fix config import installation issues. Set UUID corresponding to the settings.
bin/drush cset "system.site" uuid "26850c5a-fdbf-49a3-8a92-cca1d30cce4c" -y
bin/drush ev '\Drupal::entityManager()->getStorage("shortcut_set")->load("default")->delete();'

# Import configurations.
bin/drush cim -y

# Cache rebuild
bin/drush cr

bin/drush genu 10 --roles=administrator

# Import yaml content.
./scripts/yaml-content-import-modules.sh web/modules/content/
